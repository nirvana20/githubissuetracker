from django.shortcuts import render
from django.http import HttpResponse
import requests
from bs4 import BeautifulSoup
from datetime import datetime, timedelta,date

#url code uses for Scrapping
# url = "https://github.com/Shippable/support/issues?utm_campaign=Recruiting&utm_source=hs_email&utm_medium=email&_hsenc=p2ANqtz--SbQcGSEyiJesLBuHKlNoCiOfIWDE3MsNYpP4X0mEzpX4kEN1gmdTmNVcEbXe3ZU_sedmI12OMNmG7Rp0n0p4wXOj-kQ&_hsmi=21670750"
# url = "https://github.com/jquery/jquery/issues"
#https://github.com/Shippable/support/issues?page=2&q=is%3Aopen+is%3Aissue



def webscrapper(request):
    if request.method  == "GET":
        return render(request,'home.html', {})
    if request.method  == "POST":
        url =  request.POST.get('url')
        r = requests.get(url)

        soup = BeautifulSoup(r.content)
        issue_details = []
        # extracting all div with class = table-list-header-toggle states left
        issues = soup.find_all("div",class_="table-list-header-toggle states left")

        #finding no of issues open and closed
        pp = []
        for data in issues:
            pp = data.find_all('a')
        issues_open = pp[0].text.strip()
        issues_open = pp[0].text.strip(' ').__str__()
        issues_closed = pp[1].text.strip(' ').__str__()
        issues_open = issues_open.split()
        issues_closed = issues_closed.split()
        issues_details = []
        issues_details.append(issues_open)
        issues_details.append(issues_closed)
        date = []

        count_24_hours = 0
        count_seven_days = 0
        count_more_than_seven= 0
        for data in soup.find_all("div",class_="table-list-cell issue-title"):
            d = data.a
            issue_details.append(d.text)
            dd = data.find_all("div",class_="issue-meta")
            op = data.find_all("div",class_="issue-meta-section opened-by")
            links = data.a
            time = data.time
            date.append(time['datetime'])
        # Extracting date from date list
        for data in date:
            p =  data.split('T')
            dd = p[0].split('-')
            tt = p[1].split('Z')
            t1 = tt[0].split(':')
            ss = datetime(int(dd[0]),int(dd[1]),int(dd[2]),int(t1[0]),int(t1[1]),int(t1[2]))
            # counting number of open issues that were opened in the last 24 hours
            if ss>datetime.today()-timedelta(hours=24):
                count_24_hours = 1+count_24_hours
            # counting number of open issues that were opened more than 24 hours ago but less than 7 days ago
            if ss<datetime.today()-timedelta(hours=24) and ss.date()>datetime.today().date()-timedelta(days=7):
                count_seven_days = 1+count_seven_days
        #Number of open issues that were opened more than 7 days ago
        count_more_than_seven = int(issues_open[0]) - (count_24_hours+count_seven_days)
        return render(request, 'home.html', {"data": date, "issues_open": issues_open[0],"issues_closed": issues_closed[0],
                                             "count_24_hours":count_24_hours,"count_more_than_seven":count_more_than_seven,
                                             "count_seven_days":count_seven_days})
